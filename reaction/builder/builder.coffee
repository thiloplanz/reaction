###
//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
###

window.Reaction = {} unless window.Reaction
exports = window.Reaction



###
* A tool to build React components.
*
* It keeps track of component properties and child component,
* so that they can be easily updated.
*
* To get notified of these updates, listen to "builder.updateEvent"
* (or use the Reaction.Building(builder) mixin on your top-level component,
* or build the top-level component with "builder.renderComponent")
###


exports.Builder = class Builder
    constructor: (components)->
        @components = {}
        @updateEvent = Reflux.createAction()
        triggered = false
        @updateEvent.listen -> triggered = false
        @fireUpdate = ->
            @updateEvent.triggerAsync({}) unless triggered
            triggered = true
        for c, d of components
            @define(c, d[0], d[1], d[2])

    define: (key, comp, props, children) ->
        if @components[key]
            console.warn("redefining existing component "+key)
            @fireUpdate()
        @components[key] = [comp, props, children]
        return Ref(key)

    build: (key, moreProps) ->
        comp = @components[key]
        return errorSpan("missing component '#{key}'") unless comp
        q = comp[1]
        props = {}
        if q then for k,v of q
            props[k] = v
        if moreProps then for k,v of moreProps
            props[k] = v
        props.builder = @
        props.key = key
        props.builderKey = key

        if (comp[2] instanceof Ref)
            return React.createElement comp[0], props, @build(comp[2].key)
        return React.createElement comp[0], props, comp[2]

    update: (key, props, childElements) ->
        ckey = key.props && key.props.builderKey
        key = ckey || key
        comp = @components[key]
        unless comp
            console.error("cannot update missing component", key)
            return
        if props
            q = comp[1]
            for k,v of props
                q[k] = v
        if childElements
            comp[2] = childElements
        @fireUpdate()

    getComponentProps: (key) ->
        ckey = key.props && key.props.builderKey
        key = ckey || key
        comp = @components[key]
        unless comp
            console.error("missing component", key)
            return
        return comp[1]

    ###
    * Convenience method to have a Builder create a top-level
    * component, attach it to the DOM, and connect it for updates
    ###

    renderComponent: (key, element) ->
        if (typeof element is 'string' or element instanceof String)
            element = document.getElementById(element)
            unless element 
                alert("element '#{element}' not found")
                return        
        
        TopLevelBuilder = React.createClass
            mixins: [ Building(this) ]
            render: -> @buildComponent(key)

        return React.render(React.createElement(TopLevelBuilder), element)


###
* use Ref('componentKey') to refer to another component that the builder knows about
###

exports.Ref = class Ref
    constructor: (key) ->
        return new Ref(key) unless @ instanceof Ref
        @key = key

errorSpan = (message) -> React.DOM.span className: 'alert-danger', "[[[#{message}]]]"



###
* A mixin for React components that use a builder to create child components
###

builderAsProps = {
    buildComponent: (key, moreProps) ->
        b = @props.builder
        return if b then b.build(key,moreProps) else errorSpan("Building missing builder")

    updateBuilder: (key, moreProps) ->
        b = @props.builder
        unless b
            console.error("Building missing builder, cannot update",key)
            return
        b.update(key, moreProps)
        return   # explicit return, to sidestep React not liking false from event handlers

    getBuilderProps: (key) -> 
        b = @props.builder
        unless b
            console.error("Building missing builder, cannot get props for",key)
            return
        return b.getComponentProps(key)
}

exports.Building = Building = (builder) ->
    return builderAsProps unless builder

    x = Reflux.connect(builder.updateEvent)
    x.buildComponent = builder.build.bind(builder)
    x.updateBuilder = builder.update.bind(builder)
    x.getBuilderProps = builder.getComponentProps.bind(builder)
    return x


Building.buildComponent = builderAsProps.buildComponent;
Building.updateBuilder = builderAsProps.updateBuilder;
Building.getBuilderProps = builderAsProps.getBuilderProps;






