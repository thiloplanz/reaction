###
//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
###


window.Reaction = {} unless window.Reaction
exports = window.Reaction

DOM = React.DOM
DIV = DOM.div

# create React factories (required since React 0.12)
createFactories = (module, components...) ->
    r = {};
    for c in components
        r[c] = React.createFactory(module[c])
    return r

BS = createFactories(ReactBootstrap, 'Button', 'Glyphicon', 'MenuItem', 'ButtonGroup', 'DropdownButton', 'Input')


# A bootstrap row with two columns sized 11-1
Row11_1 = (a,b,c ) -> 
    return DIV({className: 'row'},
        DIV({className: 'col-xs-11'}, a),
        DIV({className: 'col-xs-1'}, b));

# A bootstrap row with four columns sized 8-2-1-1
Row8211 = (a,b,c,d) ->
    return DIV({className: 'row'},
        DIV({className: 'col-xs-5 col-sm-7 col-lg-8'}, a),
        DIV({className: 'col-xs-5 col-sm-3 col-lg-2 text-right'}, b),
        DIV({className: 'col-xs-1 text-right'}, c),
        DIV({className: 'col-xs-1 text-right'}, d));


# A bootstrap row with five columns sized 1-7-2-1-1
Row17211 = (a,b,c,d,e) -> 
    return DIV({className: 'row'},
        DIV({className: 'col-xs-1'}, a),
        DIV({className: 'col-xs-6 col-sm-6 col-lg-7'}, b),
        DIV({className: 'col-xs-3 col-sm-3 col-lg-2 text-right'}, c),
        DIV({className: 'col-xs-1 text-right'}, d),
        DIV({className: 'col-xs-1 text-right'}, e));



# a bootstrap glyphicon button with onClick handler (or nothing, if no handler)

exports.LargeButton = btn = (onClick, glyph) -> if onClick then BS.Button({bsSize:'large', onClick: onClick}, BS.Glyphicon({glyph: glyph})) else null

# a bootstrap glyphicon dropdown button (or nothing, if no options)
dbtn = (onClick, glyph, options) ->
    return null unless options and onClick
    names = [];
    for o in options
        names.push( BS.MenuItem({key: o.key, onClick: onClick.bind(null, o.key)}, o.title))    
    return BS.DropdownButton({bsSize:'large', pullRight: true,  title: BS.Glyphicon({glyph: glyph})},   names)


# a buttongroup [ OK, Cancel ]
exports.ButtonGroupOkayCancel = okayCancelButtons = (okay, cancel) -> BS.ButtonGroup(null, btn(okay, 'ok'), btn(cancel, 'remove'))

# some date formatting and parsing functions

_02d = (x) -> if x < 10 then "0#{x}" else x
yyyy = (x) -> if (typeof x is "string" or x instanceof String) then x.substring(0,4) else x.getFullYear() 
qqyyyy = (x) ->
    x = parseDate(x);
    m = x.getMonth();
    q = m/3 + 1;
    return "Q#{q} #{yyyy(x)}"

monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]


getMonthIdx = (x) ->
    for m, i in monthNames
        return i if m.indexOf(x) is 0
    console.warn("unknown month "+x);
    return 0


mmmyyyy = (x) ->
    x = parseDate(x);
    m = x.getMonth();
    return monthNames[m]+" "+yyyy(x)

yyyymmdd = (x) ->
    x = parseDate(x);
    return x.getFullYear() + "-" + _02d(x.getMonth()+1) + "-" + _02d(x.getDate()); 


addDays = (x, days) -> new Date(x.getFullYear(), x.getMonth(), x.getDate() + days)

startOfQuarter = (x, offset) ->
    x = new Date(x.getFullYear(), x.getMonth() - x.getMonth()%3, 1);

    while offset > 0
        x = startOfQuarter(new Date(x.getFullYear(), x.getMonth() + 3, 1));
        offset--;
    
    while offset < 0
        x = addDays(x, -1);
        x = new Date(x.getFullYear(), x.getMonth() - x.getMonth()%3, 1);
        offset++;
    
    return x;

startOfYear = (x, offset) -> new Date(x.getFullYear() + offset, 0, 1);

startOfMonth = (x, offset) -> new Date(x.getFullYear(), x.getMonth() + offset, 1);

startOfWeek = (x, offset) ->
    dow = x.getDay() - 1;  # Monday = 0
    return new Date(x.getFullYear(), x.getMonth(), x.getDate() - dow + offset * 7);

endOfQuarter = (x) -> addDays(startOfQuarter(x, 1), -1);

endOfYear = (x) -> new Date(x.getFullYear(), 11, 31);

endOfMonth = (x) -> addDays(startOfMonth(x,1), -1);

endOfWeek = (x) -> addDays(startOfWeek(x,1), -1);

parseDate = (x) -> 
    if (typeof x is "string" or x instanceof String)
        if (x.length == 4) then return new Date(x,0,1);
        return new Date(x.substring(0,4), x.substring(5,7)-1, x.substring(8,10));
    return x;

###
// FilterContainer
//
// manages a number of filters that the user can add, remove and update
// 
// collapsed: true
###

stopEvent = (event) ->
    event.preventDefault();
    event.stopPropagation();

exports.FilterContainer = React.createClass
    displayName: 'FilterContainer',
    mixins : [Reaction.Building]
    getDefaultProps: ->
        return { filters: [] , extraFilters: [], editing: {} }
    render: ->
        filters = [];
        for f,i in @props.filters
            editingSelection =  @props.editing[f];
            editing = editingSelection is '' or !!editingSelection
            overrides = { 
                container: @ 
                collapsed: @props.collapsed,
                startEdit: @filter_startEdit.bind(@, f)
                onDelete: @filter_delete.bind(@,f)
                applyEdit: @filter_applySelection.bind(@, f),
                onChange : @filter_updateEditSelection.bind(@, f),
                extraFilters: (i is @props.filters.length - 1 ) and @props.extraFilters ,
                editing: editing
            }
            if (editing) 
                overrides.selection = editingSelection; 
                overrides.cancelEdit = @filter_cancelEdit.bind(@,f);
            filters.push(@buildComponent( f, overrides))

        if (@props.collapsed)
            return DIV({ className: "alert-info"}, 
                Row11_1(
                    DOM.h4({onClick: @expand}, filters),
                    DOM.button({onClick: @expand, className:'button close'}, BS.Glyphicon({glyph: 'cog'}))
                ))
        return DOM.div( null , DOM.div({onClick: @reallyCollapse, className:'button close'}, BS.Glyphicon({glyph: 'remove'})), filters);

    expand: (container) -> @updateBuilder(@, {collapsed: false})

    reallyCollapse: -> @updateBuilder(@, {collapsed: true, editing: []})

    filter_startEdit : (filter, event) ->
        stopEvent(event);
        editing = this.props.editing || {};
        editing[filter] = this.getBuilderProps(filter).selection;
        this.updateBuilder( this, { editing: editing });

    filter_cancelEdit : (filter) ->
        editing = this.props.editing || {};
        delete editing[filter];
        this.updateBuilder( this, {editing: editing});

    filter_updateEditSelection : (filter, selection) ->
        if (selection.nativeEvent && selection.target)
            stopEvent(selection);
            selection = selection.target.value;
        editing = this.props.editing || {};
        editing[filter] = selection;
        this.updateBuilder( this, { editing: editing });

    filter_applySelection : (filter, event) -> 
        if (event && event.nativeEvent)
           stopEvent(event);
        editing = this.props.editing || {};
        this.updateBuilder( filter, { selection: editing[filter] });
        this.filter_cancelEdit(filter);

    filter_delete : (filter, event) ->
        stopEvent(event);
        filterContainer_deleteFilter(this.props.builder, this, filter);

    showFilter : (key, event) ->
        stopEvent(event);
        filterContainer_applyFilterSelection(this.props.builder, this, key);




exports.FilterContainer.applyFilterSelection = filterContainer_applyFilterSelection = (builder, container, filter, selection) ->
    if (selection) then builder.update( filter, { selection: selection });
    props = builder.getComponentProps(container);
    for f, i in props.extraFilters
        if (f is filter)
            props.filters.push(f);
            f = builder.getComponentProps(f);
            f.canDelete = true;
            props.extraFilters.splice(i,1);
            builder.update( container, { filters: props.filters, extraFilters: props.extraFilters } );
            return

exports.FilterContainer.deleteFilter = filterContainer_deleteFilter = (builder, container, filter) ->
    props = builder.getComponentProps(container);
    for f, i in props.filters
        if (f is filter)
            editing = props.editing || {};
            delete editing[filter];
            if (props.extraFilters)
                props.extraFilters.push(f) 
            else 
                props.extraFilters = [f]
            props.filters.splice(i,1);
            builder.update( container, { filters: props.filters, extraFilters: props.extraFilters, editing: editing } );
            return


render = ->
    if (@props.collapsed) then return DOM.span({className: "filter"}, @toDisplayString())
    if (@props.editing || @props.editing is '') then return @renderEditMode()
    return this.renderViewMode()


propsArray = (builder, keys) ->
    r = [];
    for k in keys
        r.push(builder.getComponentProps(k));
        r[r.length-1].key = k;
    return r

renderAddButton = (c) ->
    if (c.props.extraFilters && c.props.extraFilters.length) 
        return dbtn(c.props.container.showFilter,  'plus', propsArray(c.props.builder, c.props.extraFilters)) 
    return null

renderViewMode = ->
    onDelete = @props.canDelete && @props.onDelete;
    return DIV({className: "filter alert-info"}, Row8211(
            DOM.h4(null, this.toDisplayString()),
            BS.ButtonGroup(null,  btn(this.panLeft, 'chevron-left'), btn(this.panRight, 'chevron-right'), btn(this.props.startEdit, 'cog')),
                 btn(onDelete, 'trash'), renderAddButton(this)))

###
// DateFilter
//
// selects a date range, given as { start: yyyy-mm-dd, end: yyyy-mm-dd  }
//
###




exports.DateFilter = React.createClass 
    displayName: 'DateFilter'
    getMode: -> getSelectionModeForRange(@props.selection)
    toDisplayString: -> @getMode().toDisplayString(@)
    render: render
    renderEditMode: ->
        a = @choosePeriod;
        bt = (x) -> BS.Button({ href:'#', bsSize:'medium', onClick: a, key:x}, x)

        onDelete = this.props.canDelete && this.props.onDelete;
        weeks = [];
        thisWeek = selectionStart(this);
        threeWeeksAgo = exports.DateFilter.WEEK(-3).start
        if (thisWeek > threeWeeksAgo)
            thisWeek = threeWeeksAgo;
        
        for i in [-3 .. 2]
            weeks.push(bt(exports.DateFilter.WEEK(i, thisWeek).start));
        
        years = [];
        thisYear = Math.min( parseInt(yyyy(selectionEnd(this))), yyyy(new Date()) - 1 );
        for i in [-2 .. 1]
            years.push(bt(thisYear + i));        
        
        return DOM.div({className: "filter alert-warning"}, 
            Row17211(
                this.toDisplayString(),        
                DOM.div(null, 
                    DOM.div(null, BS.ButtonGroup(null, years), BS.ButtonGroup({className: 'pull-right'}, bt('Q1'), bt('Q2'), bt('Q3'), bt('Q4'))),
                    DOM.div(null, BS.ButtonGroup({justified: true}, bt('Jan'), bt('Feb'), bt('Mar'), bt('Apr')
                        , bt('May'), bt('Jun'), bt('Jul'), bt('Aug')
                        , bt('Sep'), bt('Oct'), bt('Nov'), bt('Dec')
                    )),
                    DOM.div(null, BS.ButtonGroup({ justified: true}, weeks))
                ),
                okayCancelButtons(this.props.applyEdit, this.props.cancelEdit),
                    btn(onDelete, 'trash'),
                     renderAddButton(this)
            ));
    
    renderViewMode: renderViewMode
    choosePeriod: (event) ->
        stopEvent(event);
        x = event.target.innerHTML;
        d = this.props.onChange;
        if (x.length == 4)  # YEAR
            return d(exports.DateFilter.YEAR(0, x)) && false;
        
        if (x.length == 2)  # QUARTER
            x = new Date(yyyy(selectionStart(this)), (x.substring(1,2)-1) * 3, 1);
            return d(exports.DateFilter.QUARTER(0, x)) && false;
        
        if (x.length == 3) # MONTH
            x = new Date(yyyy(selectionStart(this)), getMonthIdx(x), 1);
            return d(exports.DateFilter.MONTH(0, x)) && false;
        
        if (x.length == 10)  # WEEK
            x = new Date(x.substring(0,4), x.substring(5,7)-1, x.substring(8,10) );
            return d(exports.DateFilter.WEEK(0, x)) && false;

    panLeft: (event) -> 
            stopEvent(event);
            @props.onChange( @getMode()(-1, @props.selection.start)); 
            @props.applyEdit();
    panRight: (event) -> 
            stopEvent(event);
            @props.onChange( @getMode()(1, @props.selection.start)); 
            @props.applyEdit();

    
# selection modes
# call as a function to get the start and end for the previous (-1) or current (0) period

exports.DateFilter.QUARTER = (x, d) ->
    d = parseDate( d || new Date());
    start = startOfQuarter(d, x);
    return { start: yyyymmdd(start), end: yyyymmdd(endOfQuarter(start)) };

exports.DateFilter.MONTH = (x, d) ->
    d = parseDate( d || new Date());
    start = startOfMonth(d, x);
    return { start: yyyymmdd(start), end: yyyymmdd(endOfMonth(start)) };

exports.DateFilter.WEEK = (x, d) ->
    d = parseDate( d || new Date());
    start = startOfWeek(d, x);
    return { start: yyyymmdd(start), end: yyyymmdd(endOfWeek(start)) };

exports.DateFilter.YEAR = (x, d) ->
    d = parseDate( d || new Date());
    start = startOfYear(d, x);
    return { start: yyyymmdd(start), end: yyyymmdd(endOfYear(start)) };


# get the smallest selection mode (WEEK, MONTH, QUARTER or YEAR) that covers the given date range
exports.DateFilter.getSelectionModeForRange = getSelectionModeForRange = (range) ->
    s = parseDate(range.start);
    e = parseDate(range.end);
    e = (e.getTime() - s.getTime()) / (60*60*1000*24);
    if (e > 300) then return exports.DateFilter.YEAR; 
    if (e > 50) then return exports.DateFilter.QUARTER; 
    if (e > 10) then return exports.DateFilter.MONTH; 
    return exports.DateFilter.WEEK;


# check if two date ranges are the same
exports.DateFilter.sameRanges = (a, b) ->
    as = a.start;
    bs = b.start;
    ae = a.end;
    be = b.end;
    return true if (as is bs && ae is be) 
    as = yyyymmdd(as);
    bs = yyyymmdd(bs);
    ae = yyyymmdd(ae);
    be = yyyymmdd(be);
    return (as is bs && ae is be);


selectionStart = (x) ->  x.props.selection.start

selectionEnd = (x) ->  x.props.selection.end


exports.DateFilter.QUARTER.toDisplayString = (x) -> x.props.title+" in "+qqyyyy(selectionStart(x));
exports.DateFilter.YEAR.toDisplayString = (x) -> x.props.title+" in "+yyyy(selectionStart(x));
exports.DateFilter.MONTH.toDisplayString = (x) -> x.props.title+" in "+mmmyyyy(selectionStart(x));
exports.DateFilter.WEEK.toDisplayString = (x) -> x.props.title+" between "+yyyymmdd(selectionStart(x))+ " and "+yyyymmdd(selectionEnd(x));


###
// PulldownFilter
//
// select one of the given options [ {key: key, title: displayName} ,.. ]
//
###

optionTitle = (o) -> if o then o.title or o.key else null

option = (x) -> if (typeof x is "string" || x instanceof String) then { key: x } else x

exports.PulldownFilter = React.createClass
    displayName: 'PulldownFilter'
    toDisplayString: -> @props.title+" is "+optionTitle(@getSelectedOption());
    getSelectedOption: ->
        key = this.props.selection;
        for o in this.props.options
            if (o is key) then return option(key)
            if (o.key is key)
                return o;
        return null;
   
    render: render,
    renderEditMode: ->
        onDelete = this.props.canDelete && this.props.onDelete;
        options = [];
        for o in this.props.options       
            options.push(DOM.option({key: o.key, value:o.key}, optionTitle(o)));
            
        return DIV({className: "filter alert-warning"}, 
            Row8211(
                BS.Input({type:'select', value: this.props.selection, onChange: this.props.onChange, addonBefore: this.props.title}, options),
                okayCancelButtons(this.props.applyEdit, this.props.cancelEdit),
                    btn(onDelete, 'trash'),
                     renderAddButton(this)

        ));
    
    renderViewMode: renderViewMode


###
// TextInputFilter
//
// direct input into a text field
//
###

FocusMixin =     
    componentDidUpdate: (old) ->
        if @props.editing && !old.editing
           i = this.getDOMNode().getElementsByTagName('INPUT')[0];            
           i.focus();
           i.setSelectionRange(1000,1000);

exports.TextInputFilter = React.createClass
    displayName: 'TextInputFilter',
    mixins: [FocusMixin]
    toDisplayString: -> this.props.title+" is "+this.props.selection;
    render: render,
    renderEditMode: ->
        onDelete = this.props.canDelete && this.props.onDelete;
        return DOM.div({className: "filter alert-warning"}, 
            Row8211(
                BS.Input({type:'text', value: this.props.selection, onChange: this.props.onChange, addonBefore: this.props.title}),
                okayCancelButtons(this.props.applyEdit, this.props.cancelEdit),
                    btn(onDelete, 'trash'),
                     renderAddButton(this)
        ));
    
    renderViewMode: renderViewMode

  
###
// EmailFilter
//
// text field that only allows email addresses (multiple, separated by whitespace or comma or semicolon)
###

EmailInput = exports.EmailInput = React.createClass
    displayName: 'EmailInput',
    render: ->
        v = this.props.value;
        style = if this.props.valid then null else 'error'
        return BS.Input({type:'text', bsStyle: style, value: v, onChange: this.props.onChange, addonBefore: this.props.addonBefore});

exports.EmailInput.isValid = (value) ->
    if (value)
        emails = exports.EmailInput.asArray(value);
        for e in emails            
            if (e.length && !/.+\@.+\..+/.test(e))
                return false        
      
    return true;


exports.EmailInput.asArray = (value) -> if (value) then value.split(/[\s,;]+/) else []

exports.EmailInput.asString = (value) ->
    if (typeof value is "string" || value instanceof String) then return value;
    if (value) then return value.join(" ");
    return "";


exports.EmailFilter = React.createClass
    displayName: 'EmailFilter',
    mixins: [FocusMixin]
    toDisplayString: -> this.props.title+" is "+this.props.selection
    render: render,
    renderEditMode: ->
        onDelete = this.props.canDelete && this.props.onDelete;
        valid = EmailInput.isValid(this.props.selection);        
        return DOM.div({className: "filter alert-warning"}, 
            Row8211(
                React.createElement(EmailInput, {value: this.props.selection, onChange: this.props.onChange, addonBefore: this.props.title, valid: valid}),
                okayCancelButtons((if valid then this.props.applyEdit else null), this.props.cancelEdit),
                    btn(onDelete, 'trash'),
                     renderAddButton(this)

        ));
    renderViewMode: renderViewMode

###
// TypeaheadFilter
//
// text field with Twitter Typeahead plugin
//
###

Typeahead = React.createClass
    displayName: 'Typeahead',
    getDefaultProps: -> templates: 
        suggestion: (x) -> '<div>'+x.key+'</div>' 
    render: -> 
        BS.Input
            type:'text'
            className: 'form-control typeahead' 
            value: @props.value
            onChange: @props.onChange
            addonBefore: @props.title
  
    componentDidMount: ->
        element = $(this.getDOMNode()).find('.typeahead');
        $(element).typeahead(    { hint:false}, # using hint results in ReactJS error about "Two valid but unequal nodes with the same"
            {
              source: this.props.source,
              displayKey: this.props.key,
              templates: this.props.templates
            });
        $(element).on('typeahead:selected', this.props.selected);
        $(element).on('typeahead:autocompleted', this.props.selected);
    

    #componentWillUnmount: function(){
    #        var element = $(this.getDOMNode()).find('.typeahead').typeahead('destroy');
    #}


exports.TypeaheadFilter = React.createClass
    displayName: 'TypeaheadFilter',
    toDisplayString: -> this.props.title+" is "+this.props.selection;
    
    render: render,
    renderEditMode: ->
        onDelete = this.props.canDelete && this.props.onDelete;
        return DOM.div({className: "filter alert-warning"}, 
            Row8211(
                React.createElement(Typeahead,
                    {value: this.props.selection, onChange: this.props.onChange, title: this.props.title, selected: this.typeAheadSelected, source: this.props.source}),
                okayCancelButtons(this.props.applyEdit, this.props.cancelEdit),
                    btn(onDelete, 'trash'),
                     renderAddButton(this)

        ));
    
    renderViewMode: renderViewMode,
    typeAheadSelected: (query, option) -> @props.onChange(option.key);
    
    componentDidUpdate: (old) ->
        if (this.props.editing && !old.editing)
            element = $(this.getDOMNode()).find('.typeahead')[0];
            element.focus();
            element.setSelectionRange(1000,1000);
        
    


