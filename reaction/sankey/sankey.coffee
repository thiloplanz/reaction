###
//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
###

return unless window.d3


window.Reaction = {} unless window.Reaction
exports = window.Reaction

{rect, text, g, title, path, svg } = React.DOM

margin = {top: 1, right: 1, bottom: 6, left: 1};


s = exports.SankeyDiagram = React.createClass
    displayName: 'SankeyDiagram'
    getDefaultProps: -> { width: 500, height: 500, nodeWidth: 15, nodePadding: 10 }
    
    componentWillMount:  -> @componentWillReceiveProps(this.props)
    
    componentWillReceiveProps : (props) ->
        sankey = d3.sankey().nodeWidth(props.nodeWidth).nodePadding(props.nodePadding)
                .size([props.width-margin.right - margin.left, props.height- margin.bottom - margin.top]);

        p = sankey.link();
        links = props.data.links;
        nodes = props.data.nodes;
        sankey
            .nodes(nodes)
            .links(links)
            .layout(32);

        fixed = this.state?.fixedPositions

        fixed = null if fixed and fixed.size isnt nodes.length

        if (fixed) 
            for node, i in nodes
                fn = fixed[i]
                continue unless fn and fn.x is node.x
                node.y = fn.y


        @setState({ path: p, links: links, nodes: nodes, fixedPositions: fixed });
    
    render: ->
        props = this.props;
        color = d3.scale.category20();

        links = this.state.links;
        nodes = this.state.nodes; 

        svgLinks = [];
        for d,i in links
            linkStyle = d.style || {}
            linkStyle.stroke = d.color if d.color
            linkStyle.strokeWidth = Math.max(1, d.dy)

            svgLinks.push(path({className: 'link', key:'link'+i,  d: @state.path(d), style: linkStyle }, 
                (if props.linkTitleText then title(null, props.linkTitleText(d)) else null)
            ));
        

        svgLinks.sort (a,b) -> b.dy - a.dy
        
        svgNodes = [];
        nodeWidth = props.nodeWidth;
        for d,i in nodes
            d.color = d.color || color(d.name.replace(`/ .*/`, ""));
            left = d.x < props.width/2;
            oc = props.nodeOnClick && props.nodeOnClick.bind(null, d);
            svgNodes.push(g({className: 'node', key:'node'+i, transform: "translate(" + d.x + "," + d.y + ")", style : d.style}, 
                rect({height: d.dy, width: nodeWidth, onClick: oc, style: { fill: d.color, stroke: d3.rgb(d.color).darker(2) }} ),
                text({
                    x: (if left then 6 + nodeWidth else -6) 
                    y: d.dy/2
                    dy: '.35em'
                    textAnchor:  (if left then 'start' else 'end')
                    transform: null }, d.name),
                (if props.nodeTitleText then title(null, props.nodeTitleText(d)) else null)
            ));
        


        
        return svg({className: 'd3', width: props.width, height: props.height},
            g({className: 'd3-sankey', transform:  "translate(" + margin.left + "," + margin.top + ")" }, svgLinks, svgNodes)
        );


    componentDidMount: ->
        d3.select(this.getDOMNode()).selectAll('g.node').call(d3.behavior.drag()
                .origin(this.origin)
                    .on("dragstart", -> @parentNode.appendChild(@))
                    .on("drag", this.dragmove));


    origin: (d,i) -> @state.nodes[i];
    
    dragmove: (d,i) ->
        n = this.state.nodes[i];
        n.y = Math.max(0, Math.min(this.props.height - n.dy, d3.event.y));
        
        fixedPositions = this.state.fixedPositions || { size: this.state.nodes.length }
        fixedPositions[i] = { x: n.x, y: n.y }
        
        @setState({nodes: this.state.nodes, fixedPositions: fixedPositions}); 
    



s.mergeDiagramData = ->
    if arguments.length < 2 then return arguments[0]
    resultNodes = []
    resultLinks = []
    for data in arguments
        nodes = data.nodes;
        links = data.links;
        offset = resultNodes.length;
        for n in nodes 
            resultNodes.push(n)
        
        for link in links
            link.source += offset;
            link.target += offset;
            resultLinks.push(link);
        
    
    return { links: resultLinks, nodes: resultNodes };


s.collapseDuplicateNodeNames = (data) ->
    nodes = data.nodes;
    seen = {};
    links = data.links;
    for node,i  in nodes
        name = node.name;
        if (seen[name])
            nodes[i] = null;
            for link, j in links
                if (i is link.source || i is link.target)
                    if (link.source is i) then link.source = seen[name] - 1;
                    if (link.target is i) then link.target = seen[name] - 1;
                
                    # find existing link and merge
                    for k in [0 ... j] by 1
                        klink = links[k];
                        if (klink.source is link.source && klink.target is link.target)
                            klink.value += link.value;
                            links.splice(j,1);
                            j--;
                            break;
                        
                    
                
            
            continue;
        
        seen[name] = i+1;
    
    for i in [nodes.length-1 ... 0] by -1 
        unless (nodes[i])
             for link in links
                if (link.source > i) then link.source--;
                if (link.target > i) then link.target--;
             
             nodes.splice(i,1);
        
    
    return data;



