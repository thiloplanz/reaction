###
//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
###

window.Reaction = {} unless window.Reaction
exports = window.Reaction


# helper class to deal with JSON record sets as produced by jOOQ

exports.RecordSet = class RecordSet
    constructor: (data) ->
        columnMap = null
        {fields, records} = data;

        @getColumnIndex = (columnName) ->
            unless columnMap
                columnMap = {};
                for f, i in fields
                    columnMap[f.name] = i;
            return columnMap[columnName];

    get: (row, columnName) -> row[@getColumnIndex(columnName)]


